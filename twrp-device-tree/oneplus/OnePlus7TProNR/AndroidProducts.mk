#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_OnePlus7TProNR.mk

COMMON_LUNCH_CHOICES := \
    omni_OnePlus7TProNR-user \
    omni_OnePlus7TProNR-userdebug \
    omni_OnePlus7TProNR-eng
