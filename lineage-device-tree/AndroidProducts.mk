#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_OnePlus7TProNR.mk

COMMON_LUNCH_CHOICES := \
    lineage_OnePlus7TProNR-user \
    lineage_OnePlus7TProNR-userdebug \
    lineage_OnePlus7TProNR-eng
