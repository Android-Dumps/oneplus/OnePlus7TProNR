#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from OnePlus7TProNR device
$(call inherit-product, device/oneplus/OnePlus7TProNR/device.mk)

PRODUCT_DEVICE := OnePlus7TProNR
PRODUCT_NAME := lineage_OnePlus7TProNR
PRODUCT_BRAND := OnePlus
PRODUCT_MANUFACTURER := oneplus

PRODUCT_GMS_CLIENTID_BASE := android-tmus-us-revc

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="OnePlus7TProNR-user 11 RKQ1.201022.002 2110121803 release-keys"

BUILD_FINGERPRINT := OnePlus/OnePlus7TProNR/OnePlus7TProNR:11/RKQ1.201022.002/2110121803:user/release-keys
